﻿using Metier.Model.Operation;
using Metier.Model.OperationVisitor;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorTestxUNit
{
    public class OperationTestUnit
    {
        #region Add
        [Fact]
        public void AdditionTest()
        {
            var concretOp =  ConcretOperation.Instance;
            var addition = new Addition(10, 15);
           /* var lst = new List<int>();
            Parallel.For(0, 20, i => { lst.Add(i); });
            var concStack = new ConcurrentStack<int>(lst);
            concStack.ToList().ForEach(x => Debug.WriteLine(x));*/
            Assert.Equal(25, concretOp.VisiteOp(addition));
        }

        [Fact]
        public void DisplayByStringOpAdd()
        {
            var concretOp = ConcretOperation.Instance;
            var res1 = concretOp.FindOpByString("+");
            var res2 = concretOp.FindOpByString("plus");
            var res3 = concretOp.FindOpByString("add");
            var res4 = concretOp.FindOpByString(nameof(Addition));
            var res5 = concretOp.FindOpByString("blabla");
            var res6 = concretOp.FindOpByString(default);
            Assert.NotNull(res1);
            Assert.NotNull(res2);
            Assert.NotNull(res3);
            Assert.NotNull(res4);
            Assert.Null(res5);
            Assert.Null(res6);
            Assert.Equal($"{nameof(Addition)} : +, plus, add", res1?.ToString());
            Assert.Equal($"{nameof(Addition)} : +, plus, add", res2?.ToString());
            Assert.Equal($"{nameof(Addition)} : +, plus, add", res3?.ToString());
            Assert.Equal($"{nameof(Addition)} : +, plus, add", res4?.ToString());
        }

        [Fact]
        public void DoAddOperation()
        {
            var concretOp = ConcretOperation.Instance;
            Assert.Equal(10, concretOp.DoOperation("+", 5, 5));
            Assert.Equal(25, concretOp.DoOperation("plus", 20, 5));
            Assert.Equal(45, concretOp.DoOperation("add", 15, 30));
            var ex = Assert.Throws<ArgumentException>(() => concretOp.DoOperation("blabla",5,3));
            var ex2 = Assert.Throws<ArgumentNullException>(() => concretOp.DoOperation("", 3, 3));
            Assert.Equal("this operation not exist", ex.Message);
            Assert.Equal("Value cannot be null. (Parameter 'op')", ex2.Message);

        }

        #endregion Add

        #region Sub
        [Fact]
        public void SoustractionTest()
        {
            var concretOp = ConcretOperation.Instance;
            var sub = new Soustraction(20, 10);
            Assert.Equal(10, concretOp.VisiteOp(sub));
        }

        [Fact]
        public void DisplayByStringOpSub()
        {
            var concretOp = ConcretOperation.Instance;
            var res1 = concretOp.FindOpByString("-");
            var res2 = concretOp.FindOpByString("sous");
            var res3 = concretOp.FindOpByString("sub");
            var res4 = concretOp.FindOpByString(nameof(Soustraction));
            var res5 = concretOp.FindOpByString("blabla");
            var res6 = concretOp.FindOpByString(default);
            Assert.NotNull(res1);
            Assert.NotNull(res2);
            Assert.NotNull(res3);
            Assert.NotNull(res4);
            Assert.Null(res5);
            Assert.Null(res6);
            Assert.Equal($"{nameof(Soustraction)} : -,sous,sub", res1?.ToString());
            Assert.Equal($"{nameof(Soustraction)} : -,sous,sub", res2?.ToString());
            Assert.Equal($"{nameof(Soustraction)} : -,sous,sub", res3?.ToString());
            Assert.Equal($"{nameof(Soustraction)} : -,sous,sub", res4?.ToString());
        }

        [Fact]
        public void DoSubOperation()
        {
            var concretOp = ConcretOperation.Instance;
            Assert.Equal(0, concretOp.DoOperation("-", 5, 5));
            Assert.Equal(15, concretOp.DoOperation("sous", 20, 5));
            Assert.Equal(-15, concretOp.DoOperation("sub", 15, 30));
            var ex = Assert.Throws<ArgumentException>(() => concretOp.DoOperation("blabla", 5, 3));
            var ex2 = Assert.Throws<ArgumentNullException>(() => concretOp.DoOperation("", 3, 3));
            Assert.Equal("this operation not exist", ex.Message);
            Assert.Equal("Value cannot be null. (Parameter 'op')", ex2.Message);

        }

        #endregion SUb

        #region Mult
        [Fact]
        public void MultiplicationTest()
        {
            var concretOp = ConcretOperation.Instance;
            var mult = new Multiplication(5, 5);
            Assert.Equal(25, concretOp.VisiteOp(mult));
        }

        [Fact]
        public void DisplayByStringOpMult()
        {
            var concretOp = ConcretOperation.Instance;
            var res1 = concretOp.FindOpByString("*");
            var res2 = concretOp.FindOpByString("x");
            var res3 = concretOp.FindOpByString("Mult");
            var res4 = concretOp.FindOpByString(nameof(Multiplication));
            var res5 = concretOp.FindOpByString("blabla");
            var res6 = concretOp.FindOpByString(default);
            Assert.NotNull(res1);
            Assert.NotNull(res2);
            Assert.NotNull(res3);
            Assert.NotNull(res4);
            Assert.Null(res5);
            Assert.Null(res6);
            Assert.Equal($"{nameof(Multiplication)} : *,x,Mult", res1?.ToString());
            Assert.Equal($"{nameof(Multiplication)} : *,x,Mult", res2?.ToString());
            Assert.Equal($"{nameof(Multiplication)} : *,x,Mult", res3?.ToString());
            Assert.Equal($"{nameof(Multiplication)} : *,x,Mult", res4?.ToString());
        }

        [Fact]
        public void DoMultOperation()
        {
            var concretOp = ConcretOperation.Instance;
            Assert.Equal(25, concretOp.DoOperation("*", 5, 5));
            Assert.Equal(9, concretOp.DoOperation("x", 3, 3));
            Assert.Equal(30, concretOp.DoOperation("mult", 15, 2));
            var ex = Assert.Throws<ArgumentException>(() => concretOp.DoOperation("blabla", 5, 3));
            var ex2 = Assert.Throws<ArgumentNullException>(() => concretOp.DoOperation("", 3, 3));
            Assert.Equal("this operation not exist", ex.Message);
            Assert.Equal("Value cannot be null. (Parameter 'op')", ex2.Message);

        }

        #endregion Mult

        #region Div
        [Fact]
        public void DivisionTest()
        {
            var concretOp = ConcretOperation.Instance;
            var division = new Division(30, 3);
            Assert.Equal(10, concretOp.VisiteOp(division));
        }

        [Fact]
        public void DisplayByStringOpDiv()
        {
            var concretOp = ConcretOperation.Instance;
            var res1 = concretOp.FindOpByString("/");
            var res2 = concretOp.FindOpByString("div");
            var res4 = concretOp.FindOpByString(nameof(Division));
            var res5 = concretOp.FindOpByString("blabla");
            var res6 = concretOp.FindOpByString(default);
            Assert.NotNull(res1);
            Assert.NotNull(res2);
            Assert.NotNull(res4);
            Assert.Null(res5);
            Assert.Null(res6);
            Assert.Equal($"{nameof(Division)} : /,Div", res1?.ToString());
            Assert.Equal($"{nameof(Division)} : /,Div", res2?.ToString());
            Assert.Equal($"{nameof(Division)} : /,Div", res4?.ToString());
        }

        [Fact]
        public void DoDivOperation()
        {
            var concretOp = ConcretOperation.Instance;
            Assert.Equal(1, concretOp.DoOperation("/", 5, 5));
            Assert.Equal(4, concretOp.DoOperation("div", 20, 5));
            var ex = Assert.Throws<ArgumentException>(() => concretOp.DoOperation("blabla", 5, 3));
            var ex2 = Assert.Throws<ArgumentNullException>(() => concretOp.DoOperation("", 3, 3));
            var ex3 = Assert.Throws<ArgumentException>(() => concretOp.DoOperation("/", 3, 0));
            Assert.Equal("this operation not exist", ex.Message);
            Assert.Equal("Value cannot be null. (Parameter 'op')", ex2.Message);
            Assert.Equal("the divisor must be greater than 0", ex3.Message);

        }
        #endregion Div

        #region BadValue

        [Fact]
        public void BadDivision()
        {
            var concretOp = ConcretOperation.Instance;
            var division = new Division(30, 0);
            var ex = Assert.Throws<ArgumentException>(() => concretOp.VisiteOp(division));
            Assert.Equal("the divisor must be greater than 0", ex.Message);
        }


        [Fact]
        public void BadOperation()
        {
            var concretOp = ConcretOperation.Instance;
            var ex = Assert.Throws<ArgumentNullException>(() => concretOp.VisiteOp(null));
            Assert.Equal($"Value cannot be null. (Parameter '{nameof(AbstractOperation)}')", ex.Message);
        }

        #endregion BadValue
    }
}
