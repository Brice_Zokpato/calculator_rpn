﻿
using Domaine.Data;
using Domaine.Model;
using Domaine.Model.Fake;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CalculatorTestxUNit
{
    public class DataBaseTransactionTest
    {
        private readonly CalculatorContext<FakeStack> _calculatorContext;
        public DataBaseTransactionTest(CalculatorContext<FakeStack> calculatorContext) => 
            _calculatorContext = calculatorContext;

        [Fact]
        void CreateGoodData()
        {
            var data = new FakeStack() { Stack = new List<FakeContent>()};
            data.Stack.Add(new FakeContent() { value = 10});
            data.Stack.Add(new FakeContent() { value = 20});

            Assert.NotNull(_calculatorContext.Create(data));
        }
        
    }
}
