﻿using Metier.Model.Operation;

namespace Metier.Model.OperationVisitor
{
    public interface IOpVisitor
    {
        int VisiteOp(AbstractOperation abstractOperation);

        AbstractOperation? FindOpByString(string op);
        public List<string> GetAllOperation();
        int DoOperation(string op, int left, int right);
    }
}
