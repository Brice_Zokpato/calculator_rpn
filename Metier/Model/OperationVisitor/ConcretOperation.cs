﻿using Metier.Model.Operation;

namespace Metier.Model.OperationVisitor
{
    public class ConcretOperation : IOpVisitor
    {
        private static readonly IOpVisitor _instance = new ConcretOperation();
        private static readonly object o = new object();
        private  readonly List<AbstractOperation> _abstractOperations;
        private ConcretOperation() { _abstractOperations = new List<AbstractOperation>(); initListOperation(); }

        public static IOpVisitor Instance { get { lock (o) { return _instance; } } }
        public int VisiteOp(AbstractOperation abstractOperation)
        {
            if (abstractOperation == null) throw new ArgumentNullException(nameof(AbstractOperation));
            return abstractOperation.DoPeration();
        }

        private void initListOperation()
        {
            _abstractOperations.Add(new Addition(-1,-1));
            _abstractOperations.Add(new Soustraction(-1, -1));
            _abstractOperations.Add(new Multiplication(-1, -1));
            _abstractOperations.Add(new Division(-1, -1));
        }

        public AbstractOperation? FindOpByString(string op) => String.IsNullOrWhiteSpace(op) ? default :  _abstractOperations.FirstOrDefault(x => x.EqualsByString(op));

        public int DoOperation(string op, int left, int right)
        {
            if(String.IsNullOrWhiteSpace(op) ) throw new ArgumentNullException("op");
            var operation = FindOpByString(op);
            if(operation == null) throw new ArgumentException("this operation not exist");
            operation.Left = left; operation.Right = right;
            return VisiteOp(operation);
        }

        public List<string> GetAllOperation()=> _abstractOperations.Select(x => x.ToString()).ToList();
       
    }
}
