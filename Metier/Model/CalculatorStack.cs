﻿using Domaine.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Concurrent;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing.Drawing2D;
using System.Text.Json.Serialization;

namespace Metier.Model
{
    public class CalculatorStack : Base
    {
        public ICollection<Content>? Stack { get; set; }
    }

   
    public class Content : Base
    {
        public int value { get; set; }
    }
}
