﻿using Metier.Model.OperationVisitor;

namespace Metier.Model.Operation
{
    public interface IOperation
    {
        int VisitOperation(IOpVisitor visitorOp);
        public bool EqualsByString(string op);
        int DoPeration();
    }
}
