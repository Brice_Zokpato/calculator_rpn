﻿namespace Metier.Model.Operation
{
    public enum OperationEnum
    {
        Add = '+',
        Sub = '-',
        Mult = '*',
        Div = '/'
    }
}
