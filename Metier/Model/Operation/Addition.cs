﻿using Metier.Model.OperationVisitor;

namespace Metier.Model.Operation
{
    public class Addition : AbstractOperation
    {
        public Addition(int left, int right) : base(left, right)
        {
        }

        public override string Name => nameof(Addition);

        public override string Description => "Addition between two int";

        public override int DoPeration()=>Left+Right;

        public override int VisitOperation(IOpVisitor visitorOp)=> visitorOp.VisiteOp(this);

        public override string ToString()
        {
            return $"{Name} : +, plus, add";
        }
    }
}
