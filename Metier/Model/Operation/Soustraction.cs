﻿using Metier.Model.OperationVisitor;

namespace Metier.Model.Operation
{
    public class Soustraction : AbstractOperation
    {
        public Soustraction(int left, int right) : base(left, right)
        {
        }

        public override string Name => nameof(Soustraction);

        public override string Description => "Soustraction between two integers";

        public override int DoPeration()=>Left-Right;

        public override int VisitOperation(IOpVisitor visitorOp) => visitorOp.VisiteOp(this);

        public override string ToString()
        {
            return $"{Name} : -,sous,sub";
        }
    }
}
