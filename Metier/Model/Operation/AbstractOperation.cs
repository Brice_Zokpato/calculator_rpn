﻿using Metier.Model.OperationVisitor;

namespace Metier.Model.Operation
{
    public abstract class AbstractOperation : IOperation
    {
        #region Fields
        public int Left, Right;
        #endregion Fields
        #region Constructor
        public AbstractOperation(int left, int right) { Left = left; Right = right; }
        #endregion Constructor

        public abstract string Name { get; }
        public abstract string Description { get; }

        public abstract int DoPeration();

        public abstract int VisitOperation(IOpVisitor visitorOp);
        public abstract override string ToString();
        public bool EqualsByString(string op) => ToString().ToLower().Contains(op.ToLower());
    }
}
