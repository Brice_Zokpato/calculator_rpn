﻿using Metier.Model.OperationVisitor;

namespace Metier.Model.Operation
{
    public class Multiplication : AbstractOperation
    {
        public Multiplication(int left, int right) : base(left, right)
        {
        }

        public override string Name => nameof(Multiplication);

        public override string Description => "multiplication between two int";

        public override int DoPeration()=>Left*Right;

        public override int VisitOperation(IOpVisitor visitorOp) => visitorOp.VisiteOp(this);

        public override string ToString()
        {
            return $"{Name} : *,x,Mult";
        }
    }
}
