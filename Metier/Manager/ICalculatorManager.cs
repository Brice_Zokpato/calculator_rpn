﻿using Metier.Model;
using System.Collections.Concurrent;

namespace Metier.Manager
{
    public interface ICalculatorManager
    {
        Task<CalculatorStack> Create(CalculatorStack calculatorStack);
        Task<CalculatorStack> Create(int value);
        Task<CalculatorStack> Create(params int[] value);
        Task<ConcurrentStack<int>> DoOperationAsync(string stackId, string operation);
        Task<List<int>> Push(string stackId, int value);
        Task<List<int>> PushRange(string stackId, params int[] value);
        Task<List<Content>> FindStackContent(string id);
        Task<CalculatorStack> FindStackById(string stackId);
        IEnumerable<CalculatorStack> FindStacks();
        Task<bool> Delete(string stackId);
        List<string> FindAllOp();

    }
}
