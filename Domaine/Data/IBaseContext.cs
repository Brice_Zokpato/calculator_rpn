﻿namespace Domaine.Data
{
    public interface IBaseContext<T>
    {
        Task<T> Create(T value);
        Task<T?> Update(T value);
        Task<T> FindBy(Func<T, bool> predicate);
        Task<T?> FindById(string id);
        IEnumerable<T> FindManyBy(Func<T, bool> predicate);
        object FindField(Func<T, bool> predicate, Func<T, object> selectpredicate);
        IEnumerable<object> FindFields(Func<T, bool> predicate, Func<T, object> selectpredicate);
        IEnumerable<T> FindAll();
        Task<int> Delete(T value);
        int GetSize();
    }
}
