﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Domaine.Model;
using System.Data;
//using Calculator.Manager;
using Microsoft.AspNetCore.Authentication;
using System.Collections.Concurrent;
//using Microsoft.Build.Framework;

namespace Domaine.Data
{
    public class CalculatorContext<T> : DbContext, IBaseContext<T> where T : Base
    {
        public CalculatorContext (DbContextOptions<CalculatorContext<T>> options)
            : base(options)
        {
        }

      /*  protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CalculatorStack>().HasMany(calc => calc.Stack);
            modelBuilder.Entity<Content>();
        }
*/

        public DbSet<T> Base { get; set; } = default!;

        public async Task<T?> Create(T value)
        {
            if (value == null) return default;
            Add(value);
            await SaveChangesAsync();
            return value;

        }

        public async Task<T?> Update(T value)
        {
            var t = base.Update(value);
             await  SaveChangesAsync();
            return t as T;
        }

        public async Task<T?> FindBy(Func<T, bool> predicate)
        {
            var result = await Base.FindAsync("");
            return Base == default || Base.Count() == 0 ? default : await Base.FindAsync(predicate);
        }

        public async Task<T?> FindById(string id)
            => Base == default || Base.Count() == 0 ? default : await Base.FindAsync(id);

        public object FindField(Func<T, bool> predicate, Func<T, object> selectpredicate)
        {
            return Base.Where(predicate).Select(selectpredicate).First();
        }

        public IEnumerable<object> FindFields(Func<T, bool> predicate, Func<T, object> selectpredicate)
        {
            return Base.Where(predicate).Select(selectpredicate);
        }

        public IEnumerable<T> FindManyBy(Func<T, bool> predicate)
        {
            return Base.Where(predicate);
        }

        public IEnumerable<T> FindAll()
        {
            return Base.AsEnumerable();
        }

        public Task<int> Delete(T value)
        {
            Base.Remove(value);
            //Delete(value);
            return SaveChangesAsync();
        }

        public int GetSize() => Base == default ? 0 : Base.Count();
    }
}
