using Domaine.Data;
using Domaine.Model;
using Domaine.Model.Fake;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();

//app.MapGet("/", () => "Hello World!");
builder.Services.AddDbContext<CalculatorContext<FakeStack>>(options => {
    options.UseSqlite(builder.Configuration.GetConnectionString("CalculatorDBContext")
                        ?? throw new InvalidOperationException("Connection string 'CalculatorContext' not found.")
                        );
});
app.Run();
