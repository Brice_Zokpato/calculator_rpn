﻿namespace Domaine.Model.Fake
{
    public class FakeContent
    {
        public int value { get; set; }
    }
    public class FakeStack : Base
    {
        public ICollection<FakeContent>? Stack { get; set; }
    }
}
